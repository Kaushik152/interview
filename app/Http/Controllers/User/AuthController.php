<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
    public function showRegistrationForm()
    {
    	return view('auth.register');
    }

    public function register(Request $request){
    	request()->validate([
	        'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
	    ]);
	    $data=request()->all();
	    $data['password']=bcrypt(request()->password);
	    $data['ip']=request()->ip();
	    $token=str_random(40);
	    $data['token']= $token;


    	$user=User::create($data);

    	return redirect()->route('dashboard',$token)->with('status','Successfully registerd.');

    }

    public function showLoginForm(){
    	return view('auth.login');
    }

    public function login(){
    	request()->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
	    ]);

	    
	    $user=User::where(['email'=>request()->email])->first();
	    if($user){
	    	if (\Hash::check(request()->password, $user->password)) {
	    		return redirect('dashboard');
			}else{
				return redirect()->back()->withErrors(['password'=>'You have entered Wrong password.']);
			}
	    }else{
	    	return redirect()->back()->withErrors(['email'=>'Please enter vali email.']);
	    }
    }
}

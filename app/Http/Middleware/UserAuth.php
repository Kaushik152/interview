<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class UserAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token=$request->segment(2);

        $user=User::where(['token'=>$token,'ip'=>request()->ip()])->first();

        if(!$user){
            return redirect('/');
        }
        return $next($request);
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes...
Route::get('login', 'User\AuthController@showLoginForm')->name('login');
Route::post('login', 'User\AuthController@login');

Route::get('register', 'User\AuthController@showRegistrationForm')->name('register');
Route::post('register', 'User\AuthController@register');
Route::group(['middleware'=>'user'], function() 
{
	Route::get('dashboard/{token}','User\DashboardController@index')->name('dashboard');
});